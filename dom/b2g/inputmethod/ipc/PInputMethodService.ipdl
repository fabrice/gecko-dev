/* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim: set ts=8 sts=2 et sw=2 tw=80: */
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

include protocol PContent;

namespace mozilla {
namespace dom {

struct SetCompositionRequest {
  uint32_t id;
  nsString text;
};

struct EndCompositionRequest {
  uint32_t id;
  nsString text;
};

struct KeydownRequest {
  uint32_t id;
  nsString key;
};

struct KeyupRequest {
  uint32_t id;
  nsString key;
};

struct SendKeyRequest {
  uint32_t id;
  nsString key;
};

struct DeleteBackwardRequest {
  uint32_t id;
};

struct SetSelectedOptionRequest {
  uint32_t id;
  int32_t optionIndex;
};

struct SetSelectedOptionsRequest {
  uint32_t id;
  int32_t[] optionIndexes;
};

struct CommonRequest {
  uint32_t id;
  nsString requestName;
};

struct GetSelectionRangeRequest {
  uint32_t id;
};

struct GetTextRequest {
  uint32_t id;
  int32_t offset;
  int32_t length;
};

struct SetValueRequest {
  uint32_t id;
  nsString value;
};

struct ReplaceSurroundingTextRequest {
  uint32_t id;
  nsString text;
  int32_t offset;
  int32_t length;
};

struct HandleTextChangedRequest {
  nsString text;
};

struct OptionDetail {
  bool group;
  bool inGroup;
  nsString text;
  bool disabled;
  bool selected;
  bool defaultSelected;
  int32_t optionIndex;
};

struct OptionGroupDetail {
  bool group;
  nsString label;
  bool disabled;
};

union OptionDetailCollection {
  OptionDetail;
  OptionGroupDetail;
};

struct SelectionChoices {
  bool multiple;
  OptionDetailCollection[] choices;
};

struct HandleFocusRequest {
  nsString type;
  nsString inputType;
  nsString value;
  nsString max;
  nsString min;
  nsString lang;
  nsString inputMode;
  bool voiceInputSupported;
  nsString name;
  uint32_t selectionStart;
  uint32_t selectionEnd;
  SelectionChoices choices;
  nsString maxLength;
  nsString imeGroup;
  nsString lastImeGroup;
};

struct HandleBlurRequest {
  nsString imeGroup;
  nsString lastImeGroup;
};

struct SetCompositionResponse {
  uint32_t id;
  nsresult status;
};

struct EndCompositionResponse {
  uint32_t id;
  nsresult status;
};

struct KeydownResponse {
  uint32_t id;
  nsresult status;
};

struct KeyupResponse {
  uint32_t id;
  nsresult status;
};

struct SendKeyResponse {
  uint32_t id;
  nsresult status;
};

struct DeleteBackwardResponse {
  uint32_t id;
  nsresult status;
};

struct SetSelectedOptionResponse {
  uint32_t id;
  nsresult status;
};

struct SetSelectedOptionsResponse {
  uint32_t id;
  nsresult status;
};

struct CommonResponse {
  uint32_t id;
  nsresult status;
  nsString responseName;
};

struct GetSelectionRangeResponse {
  uint32_t id;
  nsresult status;
  int32_t start;
  int32_t end;
};

struct GetTextResponse {
  uint32_t id;
  nsresult status;
  nsString text;
};

union InputMethodRequest {
  SetCompositionRequest;
  EndCompositionRequest;
  KeydownRequest;
  KeyupRequest;
  SendKeyRequest;
  DeleteBackwardRequest;
  HandleFocusRequest;
  HandleBlurRequest;
  SetSelectedOptionRequest;
  SetSelectedOptionsRequest;
  CommonRequest;
  GetSelectionRangeRequest;
  GetTextRequest;
  SetValueRequest;
  ReplaceSurroundingTextRequest;
  HandleTextChangedRequest;
};

union InputMethodResponse
{
  SetCompositionResponse;
  EndCompositionResponse;
  KeydownResponse;
  KeyupResponse;
  SendKeyResponse;
  DeleteBackwardResponse;
  SetSelectedOptionResponse;
  SetSelectedOptionsResponse;
  CommonResponse;
  GetSelectionRangeResponse;
  GetTextResponse;
};

async protocol PInputMethodService {
  manager PContent;

both:
  async Request(InputMethodRequest aRequest);
  async Response(InputMethodResponse aResponse);

parent:
  async __delete__();
};

} // namespace dom
} // namespace mozilla
